FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test04.sh"]

COPY test04.sh /usr/bin/test04.sh
COPY target/test04.jar /usr/share/test04/test04.jar
